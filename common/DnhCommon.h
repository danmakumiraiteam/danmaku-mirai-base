#ifndef __TOUHOUDANMAKUFU_DNHCOMMON__
#define __TOUHOUDANMAKUFU_DNHCOMMON__

#include"DnhConstant.h"
#include "vc_vector.h"

struct ScriptInformation{
    enum{
        TYPE_UNKNOWN,
			TYPE_PLAYER,
			TYPE_SINGLE,
			TYPE_PLURAL,
			TYPE_STAGE,
			TYPE_PACKAGE,

			MAX_ID = 8,
    };
	const static wchar_t* DEFAULT;
	struct Sort;
	struct PlayerListSort;

	
	int type_;
	wchar_t* pathArchive_;
	wchar_t* pathScript_;


	wchar_t* id_;
	wchar_t* title_;
	wchar_t* text_;
	wchar_t* pathImage_;
	wchar_t* pathSystem_;
	wchar_t* pathBackground_;
	wchar_t* pathBGM_;
	
	vc_vector* listPlayer_;

	wchar_t* replayName_;

	ScriptInformation(){}
	
	int GetType(){return type_;}
	void SetType(int type){type_ = type;}
	wchar_t* GetArchivePath(){return pathArchive_;}
	void SetArchivePath(wchar_t* path){pathArchive_ = path;}
	wchar_t* GetScriptPath(){return pathScript_;}
	void SetScriptPath(wchar_t* path){pathScript_ = path;}

	wchar_t* GetID(){return id_;}
	void SetID(wchar_t* id){id_ = id;}
	wchar_t* GetTitle(){return title_;}
	void SetTitle(wchar_t* title){title_ = title;}
	wchar_t* GetText(){return text_;}
	void SetText(wchar_t* text){text_ = text;}
	wchar_t* GetImagePath(){return pathImage_;}
	void SetImagePath(wchar_t* path){pathImage_ = path;}
	wchar_t* GetSystemPath(){return pathSystem_;}
	void SetSystemPath(wchar_t* path){pathSystem_ = path;}
	void SetBackgroundPath(wchar_t* path){pathBackground_ = path;}
	wchar_t* GetBgmPath(){return pathBGM_;}
	void SetBgmPath(wchar_t* path){pathBGM_ = path;}
	vc_vector* GetPLayerList(){return listPlayer_;}
	wchar_t* GetReplayName(){return replayName_;}
	void SetReplayName(wchar_t* name){replayName_ = name;}
	vc_vector* CreatePlayerScriptInformationList();

// static ref_count_ptr
};


#endif